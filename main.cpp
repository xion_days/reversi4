//-----------------------------------------------------------------------------
//
//  File: main.cpp (~/work/reversi4/main.cpp)
//
//  Author: xion_days <xion.days_at_gmail.com>
//
//  Copyright (C) 2013 xion_days All Rights Reserved.
//
//  Create: 2013/05/24 20:48:34
//  Last update: 2013/06/08 22:13:04
//
//-----------------------------------------------------------------------------

#include <iostream>
#include <boost/format.hpp>
#include <boost/bind.hpp>
#include "unit_test.hpp"
#include "nega_max.hpp"
#include "nega_alpha.hpp"
#include "nega_scout.hpp"

//-----------------------------------------------------------------------------

int main(int argc, char * argv[])
{
  // unit test
  reversi4::UnitTest unit_test;
  const std::size_t n_trials = 100;

  std::cout << "# --- nega max counting legals ---" << std::endl;
  unit_test.test_nega_max_counting_legals();

  reversi4::NegaMax nega_max;

  std::cout << "# --- nega max ---" << std::endl;
  unit_test.test(n_trials, nega_max,
                 boost::bind(&reversi4::NegaMax::search,
                             boost::ref(nega_max),
                             reversi4::INITIAL_BLACK,
                             reversi4::INITIAL_WHITE),
                 true);

  std::cout << "# --- nega max hash ---" << std::endl;
  unit_test.test_hash(n_trials, nega_max,
                      boost::bind(&reversi4::NegaMax::search_hash,
                                  boost::ref(nega_max),
                                  reversi4::INITIAL_BLACK,
                                  reversi4::INITIAL_WHITE,
                                  _1),
                      true, 1, 16);

  reversi4::NegaAlpha nega_alpha;

  std::cout << "# --- nega alpha ---" << std::endl;
  unit_test.test(n_trials, nega_alpha,
                 boost::bind(&reversi4::NegaAlpha::search,
                             boost::ref(nega_alpha),
                             reversi4::INITIAL_BLACK,
                             reversi4::INITIAL_WHITE),
                 true);

  std::cout << "# --- nega alpha hash ---" << std::endl;
  unit_test.test_hash(n_trials, nega_alpha,
                      boost::bind(&reversi4::NegaAlpha::search_hash,
                                  boost::ref(nega_alpha),
                                  reversi4::INITIAL_BLACK,
                                  reversi4::INITIAL_WHITE,
                                  _1),
                      true, 1, 16);

  reversi4::NegaScout nega_scout;

  std::cout << "# --- nega scout ---" << std::endl;
  unit_test.test(n_trials, nega_scout,
                 boost::bind(&reversi4::NegaScout::search,
                             boost::ref(nega_scout),
                             reversi4::INITIAL_BLACK,
                             reversi4::INITIAL_WHITE),
                 true);

  std::cout << "# --- nega scout hash ---" << std::endl;
  unit_test.test_hash(n_trials, nega_scout,
                      boost::bind(&reversi4::NegaScout::search_hash,
                                  boost::ref(nega_scout),
                                  reversi4::INITIAL_BLACK,
                                  reversi4::INITIAL_WHITE,
                                  _1),
                      true, 1, 16);

  return 0;
}

//-----------------------------------------------------------------------------
