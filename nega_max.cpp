//-----------------------------------------------------------------------------
//
//  File: nega_max.cpp (~/work/reversi4/nega_max.cpp)
//
//  Author: xion_days <xion.days_at_gmail.com>
//
//  Copyright (C) 2013 xion_days All Rights Reserved.
//
//  Create: 2013/06/04 20:33:44
//  Last update: 2013/06/08 23:32:25
//
//-----------------------------------------------------------------------------

#include "nega_max.hpp"

//-----------------------------------------------------------------------------

namespace reversi4
{

//-----------------------------------------------------------------------------

// constructor/destructor
NegaMax::NegaMax()
    : m_node_count(0), m_eval_count(0), m_ref_count(0),
      m_max_legals(0), m_sum_legals(0), m_table()
{}

NegaMax::~NegaMax()
{}

//-----------------------------------------------------------------------------

// nega max search
int NegaMax::search(uint16_t black, uint16_t white)
{
  // add this node
  ++m_node_count;

  // game over
  if (util::is_board_full(black, white))
  {
    ++m_eval_count;
    return util::evaluate_board(black, white);
  }

  // legal move set
  uint16_t legals = util::generate_legals(black, white);

  // no legal move
  if (!legals)
  {
    // opponent has at least one legal move
    if (util::can_move(white, black))
    {
      return - search(white, black);
    }
    // two passes
    else
    {
      ++m_eval_count;
      return util::evaluate_board(black, white);
    }
  }

  // find best score
  int best_score = MIN_SCORE;
  while (legals)
  {
    // one move (right bit)
    const uint16_t move = util::get_one_move(legals);

    // flipped pattern
    const uint16_t flipped = util::generate_flipped(black, white, move);

    // next
    const uint16_t next_black = black | move | flipped;
    const uint16_t next_white = white ^ flipped;

    // exchange players & search next
    const int score = - search(next_white, next_black);

    // is best
    if (score > best_score)
    {
      best_score = score;
    }

    // erase this move
    legals ^= move;
  }

  return best_score;
}

int NegaMax::search_hash(uint16_t black, uint16_t white, int hash_depth)
{
  // add this node
  ++m_node_count;

  // game over
  if (util::is_board_full(black, white))
  {
    ++m_eval_count;
    return util::evaluate_board(black, white);;
  }

  // find hash
  int hash_score = MIN_SCORE;
  if (find_score(black, white, hash_score, hash_depth))
  {
    ++m_ref_count;
    return hash_score;
  }

  // legal move set
  uint16_t legals = util::generate_legals(black, white);

  // no legal move
  if (!legals)
  {
    // opponent has at least one legal move
    if (util::can_move(white, black))
    {
      return - search_hash(white, black, hash_depth - 1);
    }
    // two passes
    else
    {
      ++m_eval_count;
      return util::evaluate_board(black, white);
    }
  }

  // find best score
  int best_score = MIN_SCORE;
  while (legals)
  {
    // one move (right bit)
    const uint16_t move = util::get_one_move(legals);

    // flipped pattern
    const uint16_t flipped = util::generate_flipped(black, white, move);

    // next
    const uint16_t next_black = black | move | flipped;
    const uint16_t next_white = white ^ flipped;

    // exchange players & search next
    const int score = - search_hash(next_white, next_black, hash_depth - 1);

    // is best
    if (score > best_score)
    {
      best_score = score;
    }

    // erase this move
    legals ^= move;
  }

  // register score to table
  register_score(black, white, best_score, hash_depth);

  return best_score;
}

int NegaMax::search_counting_legals(uint16_t black, uint16_t white)
{
  // add this node
  ++m_node_count;

  // game over
  if (util::is_board_full(black, white))
  {
    ++m_eval_count;
    return util::evaluate_board(black, white);
  }

  // legal move set
  uint16_t legals = util::generate_legals(black, white);

  // the number of legals
  const std::size_t n_legals = util::count_moves(legals);
  m_max_legals = std::max(m_max_legals, n_legals);
  m_sum_legals += n_legals;

  // no legal move
  if (!legals)
  {
    // opponent has at least one legal move
    if (util::can_move(white, black))
    {
      return - search_counting_legals(white, black);
    }
    // two passes
    else
    {
      ++m_eval_count;
      return util::evaluate_board(black, white);
    }
  }

  // find best score
  int best_score = MIN_SCORE;
  while (legals)
  {
    // one move (right bit)
    const uint16_t move = util::get_one_move(legals);

    // flipped pattern
    const uint16_t flipped = util::generate_flipped(black, white, move);

    // next
    const uint16_t next_black = black | move | flipped;
    const uint16_t next_white = white ^ flipped;

    // exchange players & search next
    const int score = - search_counting_legals(next_white, next_black);

    // is best
    if (score > best_score)
    {
      best_score = score;
    }

    // erase this move
    legals ^= move;
  }

  return best_score;
}

//-----------------------------------------------------------------------------

// clear table and info
void NegaMax::clear()
{
  ScoreTable().swap(m_table);
  m_node_count = 0;
  m_eval_count = 0;
  m_ref_count = 0;
  m_max_legals = 0;
  m_sum_legals = 0;
}

// getter
std::size_t NegaMax::get_node_count() const
{
  return m_node_count;
}

std::size_t NegaMax::get_eval_count() const
{
  return m_eval_count;
}

std::size_t NegaMax::get_ref_count() const
{
  return m_ref_count;
}

std::size_t NegaMax::get_max_legals() const
{
  return m_max_legals;
}

std::size_t NegaMax::get_sum_legals() const
{
  return m_sum_legals;
}

std::size_t NegaMax::get_table_size() const
{
  return m_table.size();
}

//-----------------------------------------------------------------------------

// register search result
void NegaMax::register_score(uint16_t black, uint16_t white, int score,
                             int hash_depth)
{
  if (hash_depth > 0)
  {
    const uint32_t key = util::generate_hash_key(black, white);
    m_table[key] = score;
  }
}

// find search result
bool NegaMax::find_score(uint16_t black, uint16_t white, int & score,
                         int hash_depth)
{
  typedef ScoreTable::const_iterator iterator;

  if (hash_depth > 0)
  {
    const uint32_t key = util::generate_hash_key(black, white);
    const iterator it = m_table.find(key);

    if (it != m_table.end())
    {
      score = it->second;
      return true;
    }
  }

  return false;
}

//-----------------------------------------------------------------------------

} // namespace reversi4

//-----------------------------------------------------------------------------
