//-----------------------------------------------------------------------------
//
//  File: nega_max.hpp (~/work/reversi4/nega_max.hpp)
//
//  Author: xion_days <xion.days_at_gmail.com>
//
//  Copyright (C) 2013 xion_days All Rights Reserved.
//
//  Create: 2013/06/04 20:09:14
//  Last update: 2013/06/08 20:24:37
//
//-----------------------------------------------------------------------------

#ifndef REVERSI4_NEGA_MAX_HPP_
#define REVERSI4_NEGA_MAX_HPP_

//-----------------------------------------------------------------------------

#include "util.hpp"

//-----------------------------------------------------------------------------

namespace reversi4
{

//-----------------------------------------------------------------------------

class NegaMax
{
 public:
  // constructor/destructor
  NegaMax();
  ~NegaMax();

  // nega max search
  int search(uint16_t black, uint16_t white);
  int search_hash(uint16_t black, uint16_t white, int hash_depth);
  int search_counting_legals(uint16_t black, uint16_t white);

  // clear table and info
  void clear();

  // getter
  std::size_t get_node_count() const;
  std::size_t get_eval_count() const;
  std::size_t get_ref_count() const;
  std::size_t get_max_legals() const;
  std::size_t get_sum_legals() const;
  std::size_t get_table_size() const;

 private:
  // register search result
  void register_score(uint16_t black, uint16_t white, int score,
                      int table_depth);

  // find search result
  bool find_score(uint16_t black, uint16_t white, int & score,
                  int table_depth);

  // info
  std::size_t m_node_count;
  std::size_t m_eval_count;
  std::size_t m_ref_count;
  std::size_t m_max_legals;
  std::size_t m_sum_legals;

  // transposition table
  ScoreTable m_table;
};

//-----------------------------------------------------------------------------

} // namespace reversi4

//-----------------------------------------------------------------------------

#endif // REVERSI4_NEGA_MAX_HPP_

//-----------------------------------------------------------------------------
