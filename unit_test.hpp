//-----------------------------------------------------------------------------
//
//  File: unit_test.hpp (~/work/reversi4/unit_test.hpp)
//
//  Author: xion_days <xion.days_at_gmail.com>
//
//  Copyright (C) 2013 xion_days All Rights Reserved.
//
//  Create: 2013/05/25 22:28:05
//  Last update: 2013/06/08 21:23:55
//
//-----------------------------------------------------------------------------

#ifndef REVERSI4_UNIT_TEST_HPP_
#define REVERSI4_UNIT_TEST_HPP_

//-----------------------------------------------------------------------------

#include <iostream>
#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <boost/format.hpp>
#include "lib/posix_timer.hpp"

namespace reversi4
{

//-----------------------------------------------------------------------------

class UnitTest
{
 public:
  // constructor/destructor
  UnitTest();
  ~UnitTest();

  // test
  template <typename Searcher>
  void test(std::size_t n_trials, Searcher & searcher,
            boost::function<int ()> search, bool is_black);

  // test using table
  template <typename Searcher>
  void test_hash(std::size_t n_trials, Searcher & searcher,
                 boost::function<int (int)> search, bool is_black,
                 int min_table_depth, int max_table_depth);

  // test nega_max counting legals
  void test_nega_max_counting_legals();

 private:
  lib::posix_timer m_timer;
};

//-----------------------------------------------------------------------------

// test
template <typename Searcher>
void UnitTest::test(std::size_t n_trials, Searcher & searcher,
                    boost::function<int ()> search, bool is_black)
{
  // init
  int sum_result = 0;

  // measure time
  m_timer.restart();
  for (std::size_t i = 0; i < n_trials; ++i)
  {
    searcher.clear();
    sum_result += search();
  }

  // results
  const double time = m_timer.elapsed();
  const int result = sum_result / static_cast<int>(n_trials);
  const std::size_t node_count = searcher.get_node_count();
  const std::size_t eval_count = searcher.get_eval_count();
  const std::size_t table_size = searcher.get_table_size();
  const std::size_t ref_count = searcher.get_ref_count();

  std::cout << boost::format("%3.6f %3d %8d %8d %8d %8d")
      % time % (is_black ? result : -result)
      % node_count % eval_count % table_size % ref_count << std::endl;
}

// test using table
template <typename Searcher>
void UnitTest::test_hash(std::size_t n_trials, Searcher & searcher,
                         boost::function<int (int)> search, bool is_black,
                         int min_table_depth, int max_table_depth)
{
  for (int i = min_table_depth; i <= max_table_depth; ++i)
  {
    std::cout << boost::format("%2d ") % i;
    boost::function<int ()> search_i = boost::bind(search, i);
    test(n_trials, searcher, search_i, is_black);
  }
}

//-----------------------------------------------------------------------------

} // namespace reversi4

//-----------------------------------------------------------------------------

#endif // REVERSI4_UNIT_TEST_HPP_

//-----------------------------------------------------------------------------
