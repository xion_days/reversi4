//-----------------------------------------------------------------------------
//
//  File: util.cpp (~/work/reversi4/util.cpp)
//
//  Author: xion_days <xion.days_at_gmail.com>
//
//  Copyright (C) 2013 xion_days All Rights Reserved.
//
//  Create: 2013/06/04 20:20:49
//  Last update: 2013/06/07 22:24:08
//
//-----------------------------------------------------------------------------

#include "util.hpp"

//-----------------------------------------------------------------------------

#include <algorithm>

//-----------------------------------------------------------------------------

namespace reversi4
{

//-----------------------------------------------------------------------------

namespace
{

// helper for generate_legals
uint16_t generate_some_legals(uint16_t black, uint16_t masked_white, int dir);

// helper for generate_flipped
uint16_t generate_some_flipped(uint16_t black, uint16_t masked_white,
                               uint16_t move, int dir);

} // namespace

//-----------------------------------------------------------------------------

namespace util
{

// all bits in pattern are 1
bool is_board_full(uint16_t black, uint16_t white)
{
  return (black | white) == 0xFFFF;
}

// difference between bits of black and bits of white
int evaluate_board(uint16_t black, uint16_t white)
{
  uint32_t bits = (static_cast<uint32_t>(black) << 16) | white;

  // 2bit, 4bit, 8bit, 16bit
  bits -= (bits >> 1) & 0x55555555;
  bits = (bits & 0x33333333) + ((bits >> 2) & 0x33333333);
  bits = (bits + (bits >> 4)) & 0x0F0F0F0F;
  bits += (bits >> 8);

  // bits(black) - bits(white)
  return ((bits >> 16) & 0x000000FF) - (bits & 0x000000FF);
}

// there is at least one legal move in board
bool can_move(uint16_t black, uint16_t white)
{
  const uint16_t empties = ~(black | white);

  // diagonals & horizontal & vertical
  return ((generate_some_legals(black, white & 0x0660, 5) & empties) ||
          (generate_some_legals(black, white & 0x0660, 3) & empties) ||
          (generate_some_legals(black, white & 0x6666, 1) & empties) ||
          (generate_some_legals(black, white & 0x0FF0, 4) & empties));
}

// pattern constructed by legal moves
uint16_t generate_legals(uint16_t black, uint16_t white)
{
  // empties & (horizontal | vertical | diagonals)
  return (~(black | white)) & (generate_some_legals(black, white & 0x0660, 5) |
                               generate_some_legals(black, white & 0x0660, 3) |
                               generate_some_legals(black, white & 0x6666, 1) |
                               generate_some_legals(black, white & 0x0FF0, 4));
}

// get 1 bit at rightest position
uint16_t get_one_move(uint16_t moves)
{
  return moves & (-moves);
}

// flipped by the move
uint16_t generate_flipped(uint16_t black, uint16_t white, uint16_t move)
{
  uint16_t flipped = 0;

  // diagonals & horizontal & vertical
  flipped |= generate_some_flipped(black, white & 0x0660, move, 5);
  flipped |= generate_some_flipped(black, white & 0x0660, move, 3);
  flipped |= generate_some_flipped(black, white & 0x6666, move, 1);
  flipped |= generate_some_flipped(black, white & 0x0FF0, move, 4);

  return flipped;
}

// hash key
uint32_t generate_hash_key(uint16_t black, uint16_t white)
{
  // normal
  uint32_t min_key = (static_cast<uint32_t>(black) << 16) | white;
  uint32_t key = min_key;

  // vertical (key = vertical([upper]))
  key = ((key >> 4) & 0x0F0F0F0F) | ((key << 4) & 0xF0F0F0F0);
  key = ((key >> 8) & 0x00FF00FF) | ((key << 8) & 0xFF00FF00);
  min_key = std::min(min_key, key);

  // vertical & horizontal (key = horizontal([upper]))
  key = ((key >> 1) & 0x55555555) | ((key << 1) & 0xaaaaaaaa);
  key = ((key >> 2) & 0x33333333) | ((key << 2) & 0xcccccccc);
  min_key = std::min(min_key, key);

  // horizontal (key = vertical([upper]))
  key = ((key >> 4) & 0x0F0F0F0F) | ((key << 4) & 0xF0F0F0F0);
  key = ((key >> 8) & 0x00FF00FF) | ((key << 8) & 0xFF00FF00);
  min_key = std::min(min_key, key);

  // diagonal & horizontal (key = diagonal([upper]))
  key = ((key >> 4) & 0x0a0a0a0a) | ((key >> 1) & 0x05050505)
      | ((key << 1) & 0xa0a0a0a0) | ((key << 4) & 0x50505050);
  key = ((key >> 8) & 0x00cc00cc) | ((key >> 2) & 0x00330033)
      | ((key << 2) & 0xcc00cc00) | ((key << 8) & 0x33003300);
  min_key = std::min(min_key, key);

  // diagonal & horizontal & vertical (key = vertical([upper]))
  key = ((key >> 4) & 0x0F0F0F0F) | ((key << 4) & 0xF0F0F0F0);
  key = ((key >> 8) & 0x00FF00FF) | ((key << 8) & 0xFF00FF00);
  min_key = std::min(min_key, key);

  // diagonal & vertical (key = horizontal([upper]))
  key = ((key >> 1) & 0x55555555) | ((key << 1) & 0xaaaaaaaa);
  key = ((key >> 2) & 0x33333333) | ((key << 2) & 0xcccccccc);
  min_key = std::min(min_key, key);

  // diagonal (key = vertical([upper]))
  key = ((key >> 4) & 0x0F0F0F0F) | ((key << 4) & 0xF0F0F0F0);
  key = ((key >> 8) & 0x00FF00FF) | ((key << 8) & 0xFF00FF00);
  min_key = std::min(min_key, key);

  return min_key;
}

// the number of moves in pattern
std::size_t count_moves(uint16_t moves)
{
  // 2bit, 4bit, 8bit
  moves -= (moves >> 1) & 0x5555;
  moves = (moves & 0x3333) + (moves >> 2 & 0x3333);
  moves = (moves + (moves >> 4)) & 0x0F0F;

  return (moves + (moves >> 8)) & 0x00FF;
}

} // namespace util

//-----------------------------------------------------------------------------

namespace
{

// helper for generate_legal_moves
uint16_t generate_some_legals(uint16_t black, uint16_t masked_white, int dir)
{
  uint16_t flipped = ((black << dir) | (black >> dir)) & masked_white;
  flipped |= ((flipped << dir) | (flipped >> dir)) & masked_white;
  flipped |= ((flipped << dir) | (flipped >> dir)) & masked_white;
  return (flipped << dir) | (flipped >> dir);
}

// helper for generate_flipped_pattern
uint16_t generate_some_flipped(uint16_t black, uint16_t masked_white,
                              uint16_t move, int dir)
{
  // todo: confuse (but maybe efficient)
  const uint16_t left_white = (move << dir) & masked_white;
  const uint16_t right_white = (move >> dir) & masked_white;
  const uint16_t left_black = (black << dir) & masked_white;
  const uint16_t right_black = (black >> dir) & masked_white;

  // flipped position (2, 1, 3, 4)
  return ((left_white & (right_black | (right_black >> dir))) |
          ((left_white << dir) & right_black) |
          (right_white & (left_black | (left_black << dir))) |
          ((right_white >> dir) & left_black));
}

} // namespace

//-----------------------------------------------------------------------------

} // namespace reversi4

//-----------------------------------------------------------------------------
