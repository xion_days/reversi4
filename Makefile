#------------------------------------------------------------------------------
#
#  File: Makefile (~/work/reversi4/Makefile)
#
#  Author: xion_days <xion.days_at_gmail.com>
#
#  Copyright (C) 2013 xion_days All Rights Reserved.
#
#  Create: 2013/05/24 20:48:55
#  Last update: 2013/06/04 23:04:24
#
#------------------------------------------------------------------------------
#
#  - Makefile for C/C++
#
#------------------------------------------------------------------------------

# basic settings
TARGET := prg
ARGS :=
OBJDIR := ./obj
DEPDIR := ./dep

#------------------------------------------------------------------------------

# C++ compiler settings
CXX := g++
CXXLIBS :=
CXXINCS :=
CXXFLAGS := -g -Wall -lm -O3 #-fomit-frame-pointer -funroll-loops

#------------------------------------------------------------------------------
#------------------------------------------------------------------------------

# extension
CXXEXT := cpp
OBJEXT := o
DEPEXT := dep

#------------------------------------------------------------------------------

# modify direcoty name ("./aaa/bbb/" -> "aaa/bbb")
OBJDIR := $(shell echo $(OBJDIR) | sed -e 's/^\.\///g' | sed -e 's/\/$$//g')
DEPDIR := $(shell echo $(DEPDIR) | sed -e 's/^\.\///g' | sed -e 's/\/$$//g')

#------------------------------------------------------------------------------

# source files
SRCS := $(shell find . -type f -name "*.$(CXXEXT)" | sed -e 's/^\.\///g')
SRCDIRS := $(sort $(dir $(SRCS)))

# object files (created in $(OBJDIR))
OBJS := $(addprefix $(OBJDIR)/, $(SRCS:%.$(CXXEXT)=%.$(OBJEXT)))
OBJDIRS := $(sort $(dir $(OBJS)))

# dependancy files (created in $(DEPDIR))
DEPS := $(addprefix $(DEPDIR)/, $(SRCS:%.$(CXXEXT)=%.$(DEPEXT)))
DEPDIRS := $(sort $(dir $(DEPS)))

#------------------------------------------------------------------------------

# all
.PHONY: all
all: $(TARGET)

#------------------------------------------------------------------------------

# target
$(TARGET): $(OBJS)
	$(CXX) $(CXXFLAGS) $^ $(CXXLIBS) -o $(TARGET)

# object files
$(OBJDIR)/%.$(OBJEXT): %.$(CXXEXT)
	@(mkdir -p $(OBJDIRS))
	$(CXX) $(CXXFLAGS) -c $< $(CXXINCS) -o $@

# dependancy files
$(DEPDIR)/%.$(DEPEXT): %.$(CXXEXT)
	@(mkdir -p $(DEPDIRS))
	@(echo -n "$(OBJDIR)/" > $@)
	@(echo -n $< | sed -e 's/^\.\///g' | sed -e 's/[^\/]*$$//g' >> $@)
	$(CXX) -MM -MG $< >> $@

#------------------------------------------------------------------------------

# include dependancy files
-include $(DEPS)

#------------------------------------------------------------------------------

# clean target, objs, and deps
.PHONY: clean
clean:
	rm -rf $(DEPDIR) $(OBJDIR) $(TARGET)

# clean objs and deps
.PHONY: clean_tmp
clean_all:
	rm -rf $(DEPDIR) $(OBJDIR)

#------------------------------------------------------------------------------
