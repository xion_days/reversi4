//-----------------------------------------------------------------------------
//
//  File: unit_test.cpp (~/work/reversi4/unit_test.cpp)
//
//  Author: xion_days <xion.days_at_gmail.com>
//
//  Copyright (C) 2013 xion_days All Rights Reserved.
//
//  Create: 2013/05/25 22:58:15
//  Last update: 2013/06/08 20:14:16
//
//-----------------------------------------------------------------------------

#include "unit_test.hpp"

//-----------------------------------------------------------------------------

#include "nega_max.hpp"

//-----------------------------------------------------------------------------

namespace reversi4
{

//-----------------------------------------------------------------------------

// constructor
UnitTest::UnitTest()
    : m_timer()
{}

// destructor
UnitTest::~UnitTest()
{}

//-----------------------------------------------------------------------------

// test nega_max counting legals
void UnitTest::test_nega_max_counting_legals()
{
  // init
  NegaMax nega_max;

  const int result =
      nega_max.search_counting_legals(INITIAL_BLACK, INITIAL_WHITE);
  const std::size_t max_legals = nega_max.get_max_legals();
  const std::size_t sum_legals = nega_max.get_sum_legals();
  const std::size_t node_count = nega_max.get_node_count();
  const std::size_t eval_count = nega_max.get_eval_count();
  const double mean_legals =
      static_cast<double>(sum_legals) / static_cast<double>(node_count -
                                                            eval_count);

  std::cout << boost::format("# %3d %3.3f %8d %3d %8d %8d")
      % max_legals % mean_legals % sum_legals % result
      % node_count % eval_count << std::endl;
}

//-----------------------------------------------------------------------------

} // namespace reversi4

//-----------------------------------------------------------------------------
