#!/usr/bin/gnuplot
# -*- coding: utf-8 -*-

#------------------------------------------------------------------------------
#
#  File: plot_alpha_scout.gp (~/work/reversi4/result/plot_alpha_scout.gp)
#
#  Author: xion_days <xion.days_at_gmail.com>
#
#  Copyright (C) 2013 xion_days All Rights Reserved.
#
#  Created: 2013/06/08 23:29:31
#  Last update: 2013/06/08 23:45:39
#
#------------------------------------------------------------------------------

# --- line styles ---
# 1. (red,    dot[L],     circle)
# 2. (green,  dot[LS],    triangle)
# 3. (blue,   dot[LSS],   rectangle)
# 4. (violet, dot[MS],    diamond)
# 5. (orange, dot[MM],    triangle[reverse])
# 6. (yellow, dot[S],     cross)
set style line 1 lc rgbcolor "#D3293B" lt 2 lw 5 pt  7 ps 1.6
set style line 2 lc rgbcolor "#60AB53" lt 5 lw 5 pt  9 ps 2
set style line 3 lc rgbcolor "#3C69A0" lt 8 lw 5 pt  5 ps 1.5
set style line 4 lc rgbcolor "#8E0DFF" lt 6 lw 5 pt 13 ps 1.8
set style line 5 lc rgbcolor "#FF8F00" lt 7 lw 5 pt 11 ps 2
set style line 6 lc rgbcolor "#C09330" lt 4 lw 5 pt  2 ps 1.5

# --- title ---
set title "Comparison on Search Time"

# --- labels ---
set xlabel "hash table depth"
set ylabel "search time [sec / 100 trials]"

# --- format ---
#set format x "%1.0e"
#set format y "%1.1e"
#set logscale y

# --- ranges ---
#set xrange [0:100]
#set yrange [0:100]

# --- tics ---
#set xtics 20 scale 0
#set ytics 20 scale 0
#set grid ytics lt -1 lw 0.1

# --- legend ---
set key box right top
set key spacing 1.2

# --- plot ---
plot "result.dat" every 1 using 1:8 title "NegaAlpha" with lp ls 2
replot "result.dat" every 1 using 1:14 title "NegaScout" with lp ls 3

# --- eps mode ---
#set terminal postscript eps color enhanced "Arial" 25
set terminal pngcairo enhanced

# --- output file ---
set output "time_alpha_scout.png"
replot

# --- reset settings ---
set output
set terminal xterm

#------------------------------------------------------------------------------
