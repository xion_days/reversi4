//-----------------------------------------------------------------------------
//
//  File: posix_timer.hpp (~/work/reversi4/lib/posix_timer.hpp)
//
//  Author: xion_days <xion.days_at_gmail.com>
//
//  Copyright (C) 2013 xion_days All Rights Reserved.
//
//  Create: 2013/06/04 20:28:18
//  Last update: 2013/06/04 20:28:52
//
//-----------------------------------------------------------------------------

#ifndef REVERSI4_LIB_POSIX_TIMER_HPP_
#define REVERSI4_LIB_POSIX_TIMER_HPP_

//-----------------------------------------------------------------------------

#define BOOST_DATE_TIME_NO_LIB // unnecessary?
#include <boost/date_time/posix_time/posix_time_types.hpp>

//-----------------------------------------------------------------------------

namespace reversi4
{
namespace lib
{

//-----------------------------------------------------------------------------

// posix_timer
//  - using boost::posix_time::microsec_clock
//  - require #define BOOST_DATE_TIME_HAS_HIGH_PRECISION_CLOCK
class posix_timer
{
 public:
  // typedef
  typedef boost::posix_time::ptime ptime;
  typedef boost::posix_time::microsec_clock microsec_clock;
  typedef microsec_clock::time_duration_type time_duration_type;

 public:
  // constructor
  posix_timer();

  // default destructor
  ~posix_timer();

 public:
  // boost::timer interfaces
  void restart();
  double elapsed() const;
  double elapsed_max() const;
  double elapsed_min() const;

 private:
  ptime m_start_time;
};

//-----------------------------------------------------------------------------

// implement

// constructor
inline posix_timer::posix_timer()
    : m_start_time(microsec_clock::local_time())
{}

// default destructor
inline posix_timer::~posix_timer()
{}

//-----------------------------------------------------------------------------

// boost::timer interfaces
inline void posix_timer::restart()
{
  m_start_time = microsec_clock::local_time();
}

inline double posix_timer::elapsed() const
{
  return (microsec_clock::local_time() - m_start_time).ticks() * elapsed_min();
}

inline double posix_timer::elapsed_max() const
{
  const ptime max_time = static_cast<ptime>(boost::date_time::max_date_time);
  return (max_time - m_start_time).ticks() * elapsed_min();
}

inline double posix_timer::elapsed_min() const
{
  static const double tick_sec = 1.0 / time_duration_type::ticks_per_second();
  return tick_sec;
}

//-----------------------------------------------------------------------------

} // namespace lib
} // namespace reversi4

//-----------------------------------------------------------------------------

#endif // REVERSI4_LIB_POSIX_TIMER_HPP_

//-----------------------------------------------------------------------------
