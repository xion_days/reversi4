//-----------------------------------------------------------------------------
//
//  File: util.hpp (~/work/reversi4/util.hpp)
//
//  Author: xion_days <xion.days_at_gmail.com>
//
//  Copyright (C) 2013 xion_days All Rights Reserved.
//
//  Create: 2013/06/04 20:10:11
//  Last update: 2013/06/08 15:25:55
//
//-----------------------------------------------------------------------------

#ifndef REVERSI4_UTIL_HPP_
#define REVERSI4_UTIL_HPP_

//-----------------------------------------------------------------------------

#include <boost/cstdint.hpp>
#include <boost/unordered_map.hpp>

//-----------------------------------------------------------------------------

namespace reversi4
{

//-----------------------------------------------------------------------------

struct Bound
{
  int lower;
  int upper;
};

typedef boost::uint16_t uint16_t;
typedef boost::uint32_t uint32_t;
typedef boost::unordered_map<uint32_t, int> ScoreTable;
typedef boost::unordered_map<uint32_t, Bound> BoundTable;

//-----------------------------------------------------------------------------

const uint16_t INITIAL_BLACK = 0x0240;
const uint16_t INITIAL_WHITE = 0x0420;
const uint16_t SECOND_BLACK = 0x0270;
const uint16_t SECOND_WHITE = 0x0400;

const int MIN_SCORE = -16;
const int MAX_SCORE = +16;

//-----------------------------------------------------------------------------

namespace util
{

// all bits in pattern are 1
bool is_board_full(uint16_t black, uint16_t white);

// difference between bits of black and bits of white
int evaluate_board(uint16_t black, uint16_t white);

// there is at least one legal move in board
bool can_move(uint16_t black, uint16_t white);

// pattern constructed by legal moves
uint16_t generate_legals(uint16_t black, uint16_t white);

// get 1 bit at rightest position
uint16_t get_one_move(uint16_t moves);

// flipped by the move
uint16_t generate_flipped(uint16_t black, uint16_t white, uint16_t move);

// hash key for board
uint32_t generate_hash_key(uint16_t black, uint16_t white);

// the number of moves in pattern
std::size_t count_moves(uint16_t moves);

} // namespace util

//-----------------------------------------------------------------------------

} // namespace reversi4

//-----------------------------------------------------------------------------

#endif // REVERSI4_UTIL_HPP_

//-----------------------------------------------------------------------------
