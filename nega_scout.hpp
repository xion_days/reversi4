//-----------------------------------------------------------------------------
//
//  File: nega_scout.hpp (~/work/reversi4/nega_scout.hpp)
//
//  Author: xion_days <xion.days_at_gmail.com>
//
//  Copyright (C) 2013 xion_days All Rights Reserved.
//
//  Create: 2013/06/08 19:29:10
//  Last update: 2013/06/08 21:56:50
//
//-----------------------------------------------------------------------------

#ifndef REVERSI4_NEGA_SCOUT_HPP_
#define REVERSI4_NEGA_SCOUT_HPP_

//-----------------------------------------------------------------------------

#include "util.hpp"

//-----------------------------------------------------------------------------

namespace reversi4
{

//-----------------------------------------------------------------------------

class NegaScout
{
 public:
  // constructor/destructor
  NegaScout();
  ~NegaScout();

  // nega alpha search
  int search(uint16_t black, uint16_t white);
  int search(uint16_t black, uint16_t white, int alpha, int beta);
  int search_hash(uint16_t black, uint16_t white, int hash_depth);
  int search_hash(uint16_t black, uint16_t white, int hash_depth,
                  int alpha, int beta);

  // clear table and info
  void clear();

  // getter
  std::size_t get_node_count() const;
  std::size_t get_eval_count() const;
  std::size_t get_ref_count() const;
  std::size_t get_table_size() const;

 private:
  // register search result
  void register_bound(uint16_t black, uint16_t white,
                      int lower, int upper, int hash_depth);

  // find search result
  bool find_bound(uint16_t black, uint16_t white,
                  Bound & bound, int hash_depth);

  // info
  std::size_t m_node_count;
  std::size_t m_eval_count;
  std::size_t m_ref_count;

  // transposition table
  BoundTable m_table;
};

//-----------------------------------------------------------------------------

} // namespace reversi4

//-----------------------------------------------------------------------------

#endif // REVERSI4_NEGA_SCOUT_HPP_

//-----------------------------------------------------------------------------
