//-----------------------------------------------------------------------------
//
//  File: nega_alpha.cpp (~/work/reversi4/nega_alpha.cpp)
//
//  Author: xion_days <xion.days_at_gmail.com>
//
//  Copyright (C) 2013 xion_days All Rights Reserved.
//
//  Create: 2013/06/05 20:39:39
//  Last update: 2013/06/09 00:59:18
//
//-----------------------------------------------------------------------------

#include "nega_alpha.hpp"

//-----------------------------------------------------------------------------

namespace reversi4
{

//-----------------------------------------------------------------------------

// constructor/destructor
NegaAlpha::NegaAlpha()
    : m_node_count(0), m_eval_count(0), m_ref_count(0), m_table()
{}

NegaAlpha::~NegaAlpha()
{}

//-----------------------------------------------------------------------------

int NegaAlpha::search(uint16_t black, uint16_t white)
{
  return search(black, white, MIN_SCORE, MAX_SCORE);
}

int NegaAlpha::search(uint16_t black, uint16_t white, int alpha, int beta)
{
  // add this node
  ++m_node_count;

  // game over
  if (util::is_board_full(black, white))
  {
    ++m_eval_count;
    return util::evaluate_board(black, white);
  }

  // legal move set
  uint16_t legals = util::generate_legals(black, white);

  // no legal move
  if (!legals)
  {
    // opponent has at least one legal move
    if (util::can_move(white, black))
    {
      return - search(white, black, -beta, -alpha);
    }
    // two passes
    else
    {
      ++m_eval_count;
      return util::evaluate_board(black, white);
    }
  }

  // find best score
  while (legals)
  {
    // one move (right bit)
    const uint16_t move = util::get_one_move(legals);

    // flipped pattern
    const uint16_t flipped = util::generate_flipped(black, white, move);

    // next
    const uint16_t next_black = black | move | flipped;
    const uint16_t next_white = white ^ flipped;

    // exchange players & search next
    const int score = - search(next_white, next_black, -beta, -alpha);

    // beta cut
    if (score >= beta)
    {
      return score;
    }

    // modify alpha
    if (score > alpha)
    {
      alpha = score;
    }

    // erase this move
    legals ^= move;
  }

  return alpha;
}

int NegaAlpha::search_hash(uint16_t black, uint16_t white, int hash_depth)
{
  return search_hash(black, white, hash_depth, MIN_SCORE, MAX_SCORE);
}

int NegaAlpha::search_hash(uint16_t black, uint16_t white, int hash_depth,
                           int alpha, int beta)
{
  // add this node
  ++m_node_count;

  // game over
  if (util::is_board_full(black, white))
  {
    ++m_eval_count;
    return util::evaluate_board(black, white);
  }

  // find hash
  Bound bound;
  if (find_bound(black, white, bound, hash_depth))
  {
    ++m_ref_count;

    // beta cut & fixed score
    if (bound.lower >= beta || bound.lower == bound.upper)
    {
      return bound.lower;
    }

    // alpha cut
    if (bound.upper <= alpha)
    {
      return bound.upper;
    }

    // narrow bound
    alpha = std::max(alpha, bound.lower);
    beta = std::min(beta, bound.upper);
  }

  // legal move set
  uint16_t legals = util::generate_legals(black, white);

  // no legal move
  if (!legals)
  {
    // opponent has at least one legal move
    if (util::can_move(white, black))
    {
      return - search_hash(white, black, hash_depth - 1, -beta, -alpha);
    }
    // two passes
    else
    {
      ++m_eval_count;
      return util::evaluate_board(black, white);
    }
  }

  // find best score
  bool is_score_fixed = false;
  while (legals)
  {
    // one move (right bit)
    const uint16_t move = util::get_one_move(legals);

    // flipped pattern
    const uint16_t flipped = util::generate_flipped(black, white, move);

    // next
    const uint16_t next_black = black | move | flipped;
    const uint16_t next_white = white ^ flipped;

    // exchange players & search next
    const int score = - search_hash(next_white, next_black, hash_depth - 1,
                                    -beta, -alpha);

    // beta cut
    if (score >= beta)
    {
      register_bound(black, white, score, MAX_SCORE, hash_depth);
      return score;
    }

    // modify alpha
    if (score > alpha)
    {
      alpha = score;
      is_score_fixed = true;
    }

    // erase this move
    legals ^= move;
  }

  // register
  if (is_score_fixed)
  {
    register_bound(black, white, alpha, alpha, hash_depth);
  }
  else
  {
    register_bound(black, white, MIN_SCORE, alpha, hash_depth);
  }

  return alpha;
}

//-----------------------------------------------------------------------------

// clear table and info
void NegaAlpha::clear()
{
  BoundTable().swap(m_table);
  m_node_count = 0;
  m_eval_count = 0;
  m_ref_count = 0;
}

// getter
std::size_t NegaAlpha::get_node_count() const
{
  return m_node_count;
}

std::size_t NegaAlpha::get_eval_count() const
{
  return m_eval_count;
}

std::size_t NegaAlpha::get_ref_count() const
{
  return m_ref_count;
}

std::size_t NegaAlpha::get_table_size() const
{
  return m_table.size();
}

//-----------------------------------------------------------------------------

// register search result
void NegaAlpha::register_bound(uint16_t black, uint16_t white,
                               int lower, int upper, int hash_depth)
{
  typedef BoundTable::iterator iterator;

  if (hash_depth > 0)
  {
    // find
    const uint32_t key = util::generate_hash_key(black, white);
    const iterator it = m_table.find(key);

    // found
    if (it != m_table.end())
    {
      it->second.lower = std::max(it->second.lower, lower);
      it->second.upper = std::min(it->second.upper, upper);
    }
    // new
    else
    {
      Bound & bound = m_table[key];
      bound.lower = lower;
      bound.upper = upper;
    }
  }
}

// find search result
bool NegaAlpha::find_bound(uint16_t black, uint16_t white,
                           Bound & bound, int hash_depth)
{
  typedef BoundTable::const_iterator iterator;

  if (hash_depth > 0)
  {
    const uint32_t key = util::generate_hash_key(black, white);
    const iterator it = m_table.find(key);

    if (it != m_table.end())
    {
      bound = it->second;
      return true;
    }
  }

  return false;
}

//-----------------------------------------------------------------------------

} // namespace reversi4

//-----------------------------------------------------------------------------
